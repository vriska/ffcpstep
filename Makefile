# vi: set sw=4:

GERBERS := \
	gerber/drill_report.rpt \
	gerber/ffcpstep-CmtUser.gbr \
	gerber/ffcpstep-CuBottom.gbl \
	gerber/ffcpstep-CuTop.gtl \
	gerber/ffcpstep.drl \
	gerber/ffcpstep-drl_map.pdf \
	gerber/ffcpstep-EdgeCuts.gm1 \
	gerber/ffcpstep.gbrjob \
	gerber/ffcpstep-inner1.g2 \
	gerber/ffcpstep-inner2.g3 \
	gerber/ffcpstep-MaskBottom.gbs \
	gerber/ffcpstep-MaskTop.gts \
	gerber/ffcpstep-PasteBottom.gbp \
	gerber/ffcpstep-PasteTop.gtp \
	gerber/ffcpstep-SilkBottom.gbo \
	gerber/ffcpstep-SilkTop.gto

all: gerbers.zip $(GERBERS) bom.csv pos.csv

clean:
	rm -rf gerbers.zip gerber/ bom.csv pos.csv

.PHONY: all clean

gerbers.zip $(GERBERS): ffcpstep.kicad_pcb ffcpstep.kicad_sch
	kikit fab jlcpcb --assembly --schematic $(word 2,$^) $< .

